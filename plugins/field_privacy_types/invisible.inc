<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Invisible'),
  'weight' => 3,
  'description' => t('field is invisible to everyone'),
  'access_callback' => 'privacy_plugin_invisible_access_callback',
);

/*
 * Implement privacy plugin's access callback.
 */
function privacy_plugin_invisible_access_callback($entity, $account) {
  return FALSE;
}