<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Authenticated'),
  'weight' => 4,
  'description' => t('field is only visible for logged users'),
  'access_callback' => 'privacy_plugin_authenticated_access_callback',
);

/*
 * Implement privacy plugin's access callback.
 */
function privacy_plugin_authenticated_access_callback($entity, $account) {
  return user_is_logged_in();
}