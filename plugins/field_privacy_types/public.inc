<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Public'),
  'weight' => 1,
  'description' => t('field is visible to everyone'),
  'access_callback' => 'privacy_plugin_public_access_callback',
);

/*
 * Implement privacy plugin's access callback.
 */
function privacy_plugin_public_access_callback($entity, $account) {
  return TRUE;
}